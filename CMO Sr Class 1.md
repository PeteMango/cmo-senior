# CMO Senior Class 1

## Topics of Mathmatic Olympiads (APMO, CMO, USAMO)
- Number Theory
- Combinatorics
- Geometry
- Algebra
    1. Equality and Equation
    2. Functions
    3. Sequence

### Definitions
#### Arithmatic Mean: $`\frac{a_1+a_2+...+a_n}{n}`$
#### Geometric Mean: $`\sqrt[n]{a_1a_2...a_n}`$
#### Harmonic Mean: $`\frac{n}{\frac{1}{a_1}+\frac{1}{a_2}+...+\frac{1}{a_n}}`$
### Theorems:
Arithmatic Mean $`\ge`$ Geometric Mean <br/>
Geometric Mean $`\ge`$ Harmonic Mean <br/>
$`\therefore`$ Arithmatic Mean $`\ge`$ Geometric mean $`\ge`$ Harmonic Mean

### Questions
1. $`6a+4b+5c\ge5\sqrt{ab}+8\sqrt{ac}+3\sqrt{bc}`$ where $`a, b, c > 0`$
2. $`(1+\frac{1}{a})(1+\frac{!}{b})(1+\frac{1}{c})`$ where $`a, b, c > 0`$ and $`a + b + c = 1`$
3. $`(\frac{1+nb}{n+1})^{n+1} \ge b^n, n \in N, b > 0`$ 
4. $`(\frac{1}{n})^n<(1+\frac{1}{n+1})^(n+1), n \in N`$
5. $`\sqrt[n]{n!}<\frac{n+1}{2}, n \ge 2`$
6. Prove $`(1+\frac{b}{a})(1+\frac{c}{a})(1+\frac{a}{c}) \ge 2(1+\frac{a+b+c}{\sqrt[3]{abc}})`$ where $`a, b, c > 0`$
7. The positive reals $`a, b, c`$ satisfy $`\frac{1}{a}+\frac{1}{b}+\frac{1}{c}=1`$. Prove that $`\sqrt{a+bc}+\sqrt{b+ca}+\sqrt{c+ab} \ge \sqrt{abc}+\sqrt{a}+\sqrt{b}+\sqrt{c}`$
8. Let $`a,b,c,`$ be positive real numbers such that $`abc=8`$. Prove that $`\frac{a^2}{(1+a^3)(1+b^3)}+\frac{b^2}{(1+b^3)(1+c^3)}+\frac{c^2}{(1+c^3)(1+a^3)} \ge \frac{4}{3}`$
9. Let $`x,y,z`$ be positive real numbers such that $`\sqrt{x}+\sqrt{y}+\sqrt{z}=1`$. Prove that $`\frac{x^2+yz}{\sqrt{2x^2(z+x)}}+\frac{y^2+xz}{\sqrt{2y^2(z+x)}}+\frac{z^2+xy}{\sqrt{2z^2(x+y)}} \ge 1`$

